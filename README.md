## This is a Echo bot

This is a simple echo bot. The bot is connected to Facebook Messenger and simply echo's the same message back to you.

**Example**
```
> User: I am sunilkumar
> Bot : I am sunilkumar
> User: I like playing cricket
> Bot : I like playing cricket
```